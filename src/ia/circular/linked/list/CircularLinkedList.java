package ia.circular.linked.list;

public class CircularLinkedList<T> {
	private Node<T> first;
	private Node<T> last;

	public CircularLinkedList() {
		first = null;
		last = null;
	}

	void insertFirst(T data) {
		Node<T> newNode = new Node<>(data);
		if (isEmpty()) {
			first = newNode;
			last = newNode;
			last.setNext(first);
			return;
		}
		newNode.setNext(first);
		first = newNode;
		last.setNext(first);
	}

	void insertLast(T data) {
		Node<T> newNode = new Node<>(data);
		if (isEmpty()) {
			first = newNode;
			last = newNode;
			last.setNext(first);
			return;
		}
		newNode.setNext(first);
		last.setNext(newNode);
	}

	int deleteFirst() {
		if (isEmpty()) {
			System.out.println("emty list");
			return -1;
		}
		first = first.getNext();
		last.setNext(first);
		return 0;
	}

	boolean isEmpty() {
		return first == null;
	}
}
